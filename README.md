# README #

This speedo is meant to replace the mechanical speedo in a Datsun 1200 coupe dash.

## Components used ##
* Teensy 3.2 microcontroller
* Switec X27.168 stepper motor
* Adafruit Ultimate GPS breakout
* Adafruit 128x32 I2C OLED display
* Adafruit 32KB I2C FRAM breakout (MB85RC256V)
* 5v DC/DC switch-mode converter (TSRN 1-2450)
* 30mm Long rotary encoder with pushbutton
* 3D printed brackets
* PCB main board
* Dial face backing (actually a blank PCB - cheaper than laser cutting etc. in small runs)
* Dial face sticker
* Original Datsun 1200 speedo needle


## How to Get Started ##

### Install Arduino and Teensyduino ###
Follow the Teensyduino installation instructions.

Note: You'll need to install the Arduino IDE, but make sure you check which version Teensyduino needs.

https://www.pjrc.com/teensy/td_download.html


### Libraries bundled with Teensyduino ###
When installing Teensyduino, make sure you include at least the following libraries:

* Adafruit_GFX
* Adafruit_SSD1306
* Encoder
* TinyGPS

### External Libraries ###
You'll need to download and place the following libraries into your /Arduino/libraries/ directory:

* Adafruit_FRAM_I2C - https://github.com/adafruit/Adafruit_FRAM_I2C
* ButtonV2 - https://github.com/AndrewMascolo/ButtonV2
* SwitecX25 - https://github.com/clearwater/SwitecX25