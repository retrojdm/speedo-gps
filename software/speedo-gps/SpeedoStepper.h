#ifndef SPEEDO_STEPPER_H
#define SPEEDO_STEPPER_H

#include <Arduino.h>
#include <SwitecX25.h>
#include <IntervalTimer.h>


#define STEPPER_PIN_A1                  23
#define STEPPER_PIN_A2                  22
#define STEPPER_PIN_B1                  21
#define STEPPER_PIN_B2                  20
#define STEPPER_ACCELERATION			0.75 // [0.0..2.0], 1.0 is normal.
#define STEPPER_GAUGE_SPEED_FULL        200 // kmph
#define STEPPER_GAUGE_SWEEP_FULL        270 // degrees
#define STEPPER_SWEEP_MAX		        315 // degrees
#define STEPPER_STEPS_PER_DEGREE		3
#define STEPPER_MAXSTEP                 STEPPER_SWEEP_MAX * STEPPER_STEPS_PER_DEGREE

#define STEPPER_SMOOTHING_FRAMES		6
#define STEPPER_SMOOTHING_FPS			10 // Hz
#define STEPPER_SMOOTHING_DELAY			1000 / STEPPER_SMOOTHING_FPS // ms

#define STEPPER_MODE_STARTUP_INIT		0
#define STEPPER_MODE_STARTUP_GOINGUP	1
#define STEPPER_MODE_STARTUP_GOINGDOWN	2
#define STEPPER_MODE_NORMAL				3

// We need to forward declare the Speedo class, since it contains
// the button class as a member.
class Speedo;


// Instead of updating the stepper motor in the loop, we set up an interval timer to do it in an interrupt.
// This means smoother needle movement, since updating the relatively long update to the OLED display
// doesn't block the call to update the stepper.
//
// Note:
//	The Switec X27.168 is rated to:
//		* 600 degrees-per-second (with appropriate acceleration).
//		* 1/3� resolution per partial step
//
//	So, 1800 steps per second, or 555.55 microseconds between steps.
//
//	In practice, 1000 microseconds seems to be the minimum delay before the motor skips steps.
//	
class SpeedoStepper
{
public:
	SpeedoStepper(Speedo * speedo);
	void begin(void(*stepperCallbackFunction)());
	void loop();
	void update();
	void indicateSpeed(int kmph);

private:
	Speedo			*_speedo;
	SwitecX25		_stepper;
	IntervalTimer	_stepperTimer;
	int				_mode;

	unsigned long	_smoothingTimer;
	int				_smoothingFrame;
	int				_smoothing[STEPPER_SMOOTHING_FRAMES];

	void			_handleMode			();
	void			_handleSmoothing	();
};

#endif