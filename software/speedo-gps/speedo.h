#ifndef SPEEDO_H
#define SPEEDO_H

// Serial Monitor needs to be at 57600 baud.
#define SERIAL_MONITOR

// Uncomment this line if you want to see what the GPS is outputting.
#define ECHO_GPS_TO_SERIAL_MONITOR

// Uncomment this line if you want to see what the GPS is outputting.
#define ECHO_ENCODER_POSITION

// Uncomment this line if you want to see what mode the stepper is in.
#define ECHO_STEPPER_MODE


#include <Arduino.h>

#include "SpeedoButton.h"
#include "SpeedoDisplay.h"
#include "SpeedoEncoder.h"
#include "SpeedoFRAM.h"
#include "SpeedoGPS.h"
#include "SpeedoSettings.h"
#include "SpeedoStepper.h"


#define SCREENS				6
#define SCREEN_ODOMETER		0
#define SCREEN_TRIP_COUNTER	1
#define SCREEN_CLOCK		2
#define SCREEN_GPS			3
#define SCREEN_SPEED		4
#define SCREEN_SPEED_PEAK	5

#define MODE_VIEW			0
#define MODE_GPS_MORE		1
#define MODE_CLOCK_MINUTES	2
#define MODE_CLOCK_HOURS	3


class Speedo {
public:
	Speedo();
	void begin(void(*stepperCallbackFunction)());
	void loop();

	// Related classes.
	SpeedoButton	button;
	SpeedoFRAM		fram;
	SpeedoDisplay	display;
	SpeedoEncoder	encoder;
	SpeedoGPS		gps;
	SpeedoStepper	stepper;

	// Settings.
	SpeedoSettings	settings;

	// State.
	byte			mode;
	unsigned long	hdop;
	byte			satellites;
	unsigned long	age;
	bool			fix;
	int				kmph;
	float			latitude;   // degrees from equator         (positive = north, negative = south)
	float			longitude;  // degrees from prime meridian  (positive = east, negative = west)
	float			altitude;   // cm
	float			course;     // degrees (north = 0, east = 90, south = 180, west = 270) 
	byte			hour;
	byte			minute;
	byte			second;

	void			(*stepperCallback)();
};


// Static Functions.
int	absolute	(int x);
int	mod			(int x, int m);

#endif
