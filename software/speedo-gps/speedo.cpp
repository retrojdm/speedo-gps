#include "Speedo.h"


Speedo::Speedo()
	: button(this), display(this), encoder(this), fram(this), gps(this), settings(), stepper(this)
{
	mode = MODE_VIEW;

	hdop		= TinyGPS::GPS_INVALID_HDOP;
	satellites	= TinyGPS::GPS_INVALID_SATELLITES;
	kmph		= 0;
	latitude	= TinyGPS::GPS_INVALID_F_ANGLE;
	longitude	= TinyGPS::GPS_INVALID_F_ANGLE;
	altitude	= TinyGPS::GPS_INVALID_F_ALTITUDE;
	course		= TinyGPS::GPS_INVALID_F_ANGLE;
	hour		= 0;
	minute		= 0;
	second		= 0;
}


void Speedo::begin(void(*stepperCallbackFunction)())
{
	#ifdef SERIAL_MONITOR
		Serial.begin(57600);
		Serial.println("RetroJDM.com GPS Speedo");
	#endif

	// We need the FRAM settings first.
	fram.begin();

	// Outputs next, since it's what the user will see.
	display.begin();
	stepper.begin(stepperCallbackFunction);

	// Inputs last.
	button.begin();
	encoder.begin();
	gps.begin();
}


void Speedo::loop()
{
	// Do the Button, Encoder and GPS stuff first, since they're inputs.
	button.loop();
	encoder.loop();
	gps.loop();

	// Do the Display and Stepper stuff last, since they're outputs.
	display.loop();
	stepper.loop();
}




////////////////////////////////////////////////////////////////////////////////
// Static Functions

// We can't use abs() because it's defined as a macro that breaks in Visual Micro.
int absolute(int x)
{
	return (x > 0) ? x : -x;
}


// Taken from "Mod of negative number is melting my brain" om stackoverflow.com
// http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain
int mod(int x, int m) {
	int r = x%m;
	return r<0 ? r + m : r;
}