#ifndef SPEEDO_BUTTON_H
#define SPEEDO_BUTTON_H

#include <Arduino.h>
#include <ButtonV2.h>
#include "SpeedoSettings.h"
#include "SpeedoFRAM.h"

#define BUTTON_PIN 2


// We need to forward declare the Speedo class, since it contains
// the button class as a member.
class Speedo;


class SpeedoButton
{
public:
	SpeedoButton(Speedo * speedo);
	void		begin	();
	void		loop	();

private:
	Speedo		*_speedo; // Pointer to the parent Speedo class, for access to settings and related function etc.
	ButtonV2	_button;
	bool		_heldHandled;

	void		_onPressed				();
	void		_onHeld					();

	void		_odometerOnPressed		();
	void		_odometerOnHeld			();
	void		_tripCounterOnPressed	();
	void		_tripCounterOnHeld		();	
	void		_clockOnPressed			();
	void		_clockOnHeld			();
	void		_gpsOnPressed			();
	void		_gpsOnHeld				();
	void		_speedOnPressed			();
	void		_speedOnHeld			();
	void		_speedPeakOnPressed		();
	void		_speedPeakOnHeld		();
};

#endif
