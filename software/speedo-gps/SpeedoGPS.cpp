#include "SpeedoGPS.h"


// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "speedo.h"


SpeedoGPS::SpeedoGPS(Speedo * speedo)
	: _gps()
{
	_speedo = speedo;
	_clockTimer	= 0;
	_lastFix	= 0;
}


void SpeedoGPS::begin()
{
	delay(GPS_INIT_DELAY);

	Serial2.begin(9600);
	Serial2.println(PMTK_SET_BAUD_57600);
	Serial2.end();
	Serial2.begin(57600);

	Serial2.println(PMTK_SET_NMEA_OUTPUT_RMCGGA);
	Serial2.println(PMTK_SET_NMEA_UPDATE_5HZ);
}


void SpeedoGPS::loop()
{
	// Read the GPS data.
	bool newData = false;
	while (Serial2.available())
	{
		char c = Serial2.read();
		
		#ifdef DEBUG
			Serial.write(c);
		#endif

		if (_gps.encode(c))
			newData = true;
	}

	if (newData)
		_parse();

	_updateClock();
	_checkIfStillFixed();
}


// Tries to update the speed and position attributes of the state object.
void SpeedoGPS::_parse()
{
	float flat, flon;
	unsigned long age;

	// Get the status from the GPS.
	_gps.f_get_position(&flat, &flon, &age);
	_speedo->hdop		= _gps.hdop();
	_speedo->satellites	= _gps.satellites();
	_speedo->age		= age;

	// Do we have a "fix" ?
	_speedo->fix =
		(_speedo->hdop != TinyGPS::GPS_INVALID_SATELLITES) &&
		(_speedo->hdop <= GPS_MAXIMUM_HDOP) &&
		(_speedo->satellites != TinyGPS::GPS_INVALID_SATELLITES) &&
		(_speedo->satellites >= GPS_MINIMUM_SATELLITES) &&
		(age != TinyGPS::GPS_INVALID_AGE);

	// Do we have a fix?
	if (_speedo->fix)
	{
		_lastFix = millis();

		// Speed
		int	oldkmph	= _speedo->kmph;
		float fkmph = _gps.f_speed_kmph();
		if (fkmph == TinyGPS::GPS_INVALID_F_SPEED) fkmph = 0;
		int	kmph = fkmph;
		_speedo->kmph = kmph;
		_checkPeakKmph(kmph);

		// Remember last poistion.
		float lastLatitude	= _speedo->latitude;
		float lastLongitude	= _speedo->longitude;

		// Position        
		_speedo->latitude	= flat;
		_speedo->longitude	= flon;
		_speedo->altitude	= _gps.f_altitude();
		_speedo->course		= _gps.f_course();

		// Distance between the position at the last fix and the current position (in meters).
		if (
			(lastLatitude != TinyGPS::GPS_INVALID_F_ANGLE) &&
			(lastLongitude != TinyGPS::GPS_INVALID_F_ANGLE) &&
			(flat != TinyGPS::GPS_INVALID_F_ANGLE) &&
			(flon != TinyGPS::GPS_INVALID_F_ANGLE))
		{
			float distanceTravelled =
				TinyGPS::distance_between(lastLatitude, lastLongitude, flat, flon);

			// The distance travelled can be quite inaccurate, especially since
			// The GPS jumps around alot when getting a fix.
			// We only add it to the odometer if we're satisfied it's accurate.
			if ((distanceTravelled > 0) && (age <= GPS_MAX_AGE))
				_incrementOdometer((unsigned long)distanceTravelled);
		}
	}
}


// Reads the clock from the GPS and updates the state object.
void SpeedoGPS::_updateClock()
{
	// Refresh the clock?
	if (millis() < _clockTimer) return;
	_clockTimer = millis() + (GPS_CLOCK_DELAY);

	int year;
	byte month, day, hour, minute, second, hundredths;
	unsigned long age;
	_gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);

	if (age != TinyGPS::GPS_INVALID_AGE)
	{
		_speedo->hour	= hour;
		_speedo->minute	= minute;
		_speedo->second	= second;
	}
}


// If it's been too long since we last received good GPS data, we consider the fix lost.
void SpeedoGPS::_checkIfStillFixed()
{
	if (millis() - _lastFix > GPS_MAX_AGE)
	{
		_speedo->fix = false;
		_speedo->kmph = 0;
	}
}


// Takes the distance travelled since the last gps update, and adds it to
// the odomoeter and trip counter.
void SpeedoGPS::_incrementOdometer(unsigned long meters)
{
	this->_speedo->settings.odometer += meters;
	this->_speedo->settings.tripCounter += meters;
	this->_speedo->fram.saveSettings();
}


// Is this the peak speed?
void SpeedoGPS::_checkPeakKmph(int kmph)
{
	if (kmph > (_speedo->settings).kmphPeak)
	{
		_speedo->settings.kmphPeak = kmph;
		_speedo->fram.saveSettings();
	}
}