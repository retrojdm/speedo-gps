#ifndef SPEEDO_SETTINGS_H
#define SPEEDO_SETTINGS_H

#include <Arduino.h>


#define SETTINGS_VERSION "GPSv0.6" // int kmphPeak


class SpeedoSettings
{
public:
	SpeedoSettings();

	char			version[8];
	byte			screen;
	int				gmtHours;
	int				gmtMinutes;
	unsigned long	odometer;
	unsigned long	tripCounter;
	int				kmphPeak;
};

#endif
