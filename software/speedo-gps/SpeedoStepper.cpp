#include "SpeedoStepper.h"


// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "speedo.h"


// Class constructor.
// A pointer to the gauge that this stepper belongs to is passed as an argument.
// Initialises an instance of the SwitecX25 object.
SpeedoStepper::SpeedoStepper(Speedo * speedo)
	: _stepper(STEPPER_MAXSTEP, STEPPER_PIN_A1, STEPPER_PIN_A2, STEPPER_PIN_B1, STEPPER_PIN_B2)
{
	_speedo = speedo;
}


// Init the stepper.
// The callback function is passed as the only parameter.
void SpeedoStepper::begin(void(*stepperCallbackFunction)())
{
	_stepper.setAccel(STEPPER_ACCELERATION);

	// Set up an IntervalTimer (interrupt) to update the stepper motor.
	_stepperTimer.begin(stepperCallbackFunction, RESET_STEP_MICROSEC);

	// Init smoothing.
	_smoothingTimer = 0;
	_smoothingFrame = 0;
	for (int i = 0; i < STEPPER_SMOOTHING_FRAMES; i++)
		_smoothing[i] = 0;

	_mode = STEPPER_MODE_STARTUP_INIT;
	#ifdef ECHO_STEPPER_MODE
		Serial.print("Mode: INIT");
	#endif
}


// Handle the stepper.
void SpeedoStepper::loop()
{
	_handleMode();	
	_handleSmoothing();
}


// We expose a public update function that can be called by our static IntervalTimer wrapper function.
void SpeedoStepper::update()
{
	_stepper.update();
}


// Sets the target position of the stepper motor, in KMPH.
void SpeedoStepper::indicateSpeed(int kmph)
{
	if (_mode != STEPPER_MODE_NORMAL) return;

	#ifdef ECHO_STEPPER_MODE
		char text[51];
		sprintf(text, "indicateSpeed(%d)", kmph);
		Serial.println(text);
	#endif

	// Show speed in kmph.
	int targetPosition = map(
		kmph,
		0, STEPPER_GAUGE_SPEED_FULL,
		0, STEPPER_GAUGE_SWEEP_FULL * STEPPER_STEPS_PER_DEGREE);

	_stepper.setPosition(targetPosition);
}


// The startup sequence is achieved by treating the difference phases as modes.
// The trigger to go to the next phase (or "mode") is when the stepper reaches it's target.
void SpeedoStepper::_handleMode()
{
	if (_stepper.currentStep != _stepper.targetStep)
		return;

	switch (_mode)
	{
	case STEPPER_MODE_STARTUP_INIT:
		_stepper.setPosition(STEPPER_MAXSTEP);
		_mode = STEPPER_MODE_STARTUP_GOINGUP;
		#ifdef ECHO_STEPPER_MODE
			Serial.print("Mode: GOINGUP");
		#endif
		break;

	case STEPPER_MODE_STARTUP_GOINGUP:
		_stepper.setPosition(0);
		_mode = STEPPER_MODE_STARTUP_GOINGDOWN;
		#ifdef ECHO_STEPPER_MODE
			Serial.print("Mode: GOINGDOWN");
		#endif
		break;

	case STEPPER_MODE_STARTUP_GOINGDOWN:
		_mode = STEPPER_MODE_NORMAL;
		#ifdef ECHO_STEPPER_MODE
			Serial.print("Mode: NORMAL");
		#endif
		break;

	case STEPPER_MODE_NORMAL:
		// Do nothing
		break;
	}
}


// Handles the timer, and smoothing index.
void SpeedoStepper::_handleSmoothing()
{
	// Handle the timing.
	if (millis() < _smoothingTimer) return;
	_smoothingTimer = millis() + STEPPER_SMOOTHING_DELAY;

	// Save the Kmph at this moment, and move the index.
	_smoothing[_smoothingFrame] = _speedo->kmph;
	_smoothingFrame++;
	if (_smoothingFrame >= STEPPER_SMOOTHING_FRAMES)
		_smoothingFrame = 0;

	// Get the average.
	long totalKmph = 0;
	for (int i = 0; i < STEPPER_SMOOTHING_FRAMES; i++)
		totalKmph += _smoothing[i];
	int averageKmph = totalKmph / STEPPER_SMOOTHING_FRAMES;

	// Move the needle.
	indicateSpeed(averageKmph);
}