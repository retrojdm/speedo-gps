#include "SpeedoButton.h"

// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "Speedo.h"

SpeedoButton::SpeedoButton(Speedo * speedo)
	: _button()
{
	_speedo = speedo;
}


void SpeedoButton::begin()
{
	pinMode(BUTTON_PIN, INPUT_PULLUP);

	// Set what the button will be when pressed (default is HIGH)
	// and set the hold time (default is 500)
	_button.SetStateAndTime(LOW);
	_heldHandled = false;
}


void SpeedoButton::loop()
{
	switch (_button.CheckButton(BUTTON_PIN))
	{
	case WAITING:
		_heldHandled = false;
		break;

	case PRESSED:
		_onPressed();
		break;
	
	case HELD:
		if (!_heldHandled)
		{
			_onHeld();
			_heldHandled = true;
		}
		break;
	}
}


////////////////////////////////////////////////////////////////////////////////
// Private Functions

void SpeedoButton::_onPressed()
{
	switch (_speedo->settings.screen)
	{
	case SCREEN_ODOMETER:		_odometerOnPressed();		break;
	case SCREEN_TRIP_COUNTER:	_tripCounterOnPressed();	break;
	case SCREEN_CLOCK:			_clockOnPressed();			break;
	case SCREEN_GPS:			_gpsOnPressed();			break;
	case SCREEN_SPEED:			_speedOnPressed();			break;
	case SCREEN_SPEED_PEAK:		_speedPeakOnPressed();		break;
	}
}


void SpeedoButton::_onHeld()
{
	switch (_speedo->settings.screen)
	{
	case SCREEN_ODOMETER:		_odometerOnHeld();		break;
	case SCREEN_TRIP_COUNTER:	_tripCounterOnHeld();	break;
	case SCREEN_CLOCK:			_clockOnHeld();			break;
	case SCREEN_GPS:			_gpsOnHeld();			break;
	case SCREEN_SPEED:			_speedOnHeld();			break;
	case SCREEN_SPEED_PEAK:		_speedPeakOnHeld();		break;
	}
}


////////////////////////////////////////////////////////////////////////////////
// Odometer

void SpeedoButton::_odometerOnPressed()
{

}


void SpeedoButton::_odometerOnHeld()
{

}


////////////////////////////////////////////////////////////////////////////////
// Trip Counter

void SpeedoButton::_tripCounterOnPressed()
{

}


void SpeedoButton::_tripCounterOnHeld()
{
	_speedo->settings.tripCounter = 0;
	_speedo->fram.saveSettings();
}


////////////////////////////////////////////////////////////////////////////////
// Clock

void SpeedoButton::_clockOnPressed()
{
	switch (_speedo->mode)
	{
	case MODE_CLOCK_HOURS:		_speedo->mode = MODE_CLOCK_MINUTES;	break;
	case MODE_CLOCK_MINUTES:	_speedo->mode = MODE_CLOCK_HOURS;	break;
	}
}


void SpeedoButton::_clockOnHeld()
{
	switch (_speedo->mode)
	{
	case MODE_CLOCK_HOURS:
	case MODE_CLOCK_MINUTES:
		_speedo->mode = MODE_VIEW;
		break;
	
	case MODE_VIEW:
		_speedo->mode = MODE_CLOCK_HOURS;
		break;
	}
}


////////////////////////////////////////////////////////////////////////////////
// GPS

void SpeedoButton::_gpsOnPressed()
{
	_speedo->mode = (_speedo->mode == MODE_VIEW)
		? MODE_GPS_MORE
		: MODE_VIEW;
}


void SpeedoButton::_gpsOnHeld()
{

}


////////////////////////////////////////////////////////////////////////////////
// Speed

void SpeedoButton::_speedOnPressed()
{

}


void SpeedoButton::_speedOnHeld()
{

}


////////////////////////////////////////////////////////////////////////////////
// Speed Peak

void SpeedoButton::_speedPeakOnPressed()
{

}


void SpeedoButton::_speedPeakOnHeld()
{
	_speedo->settings.kmphPeak = 0;
	_speedo->fram.saveSettings();
}