#include "SpeedoFRAM.h"


// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "speedo.h"


SpeedoFRAM::SpeedoFRAM(Speedo * speedo)
	: _fram()
{
	_speedo = speedo;
}


void SpeedoFRAM::begin()
{
	_detected = _fram.begin();
	loadSettings();
}


void SpeedoFRAM::loop()
{

}


void SpeedoFRAM::saveSettings() {

	if (!_detected) return;

	// The address in FRAM where the settings will be saved.
	unsigned int address = FRAM_SETTINGS_ADDRESS;

	// get a byte pointer that points to the beginning of the struct    
	uint8_t *pointer = (uint8_t*)&(_speedo->settings);

	// Serialize the struct, and write it byte-by-byte to FRAM.
	for (unsigned int i = 0; i < sizeof(_speedo->settings); i++) _fram.write8(address++, *pointer++);
}



void SpeedoFRAM::loadSettings() {

	if (_detected)
	{
		// The address in FRAM where the settings will be saved.
		unsigned int address = FRAM_SETTINGS_ADDRESS;

		// get a byte pointer that points to the beginning of the struct    
		uint8_t *pointer = (uint8_t*)&(_speedo->settings);

		// Serialize the struct, and read it byte-by-byte from FRAM.
		for (unsigned int i = 0; i < sizeof(_speedo->settings); i++) *pointer++ = _fram.read8(address++);
	}

	// Not ID'd as GPS Speedo Settings?
	// Use the default settings.
	if (strcmp(_speedo->settings.version, SETTINGS_VERSION) != 0) {

		strcpy(_speedo->settings.version, SETTINGS_VERSION);
		_speedo->settings.screen		= 0;
		_speedo->settings.gmtHours		= 10;	// AEST
		_speedo->settings.gmtMinutes	= 0;	// AEST
		_speedo->settings.odometer		= 0;
		_speedo->settings.tripCounter	= 0;
		_speedo->settings.kmphPeak		= 0;
	}
}
