#ifndef SPEEDO_GPS_H
#define SPEEDO_GPS_H

#include <Arduino.h>
#include <TinyGPS.h>


#define GPS_INIT_DELAY			500		// ms
#define GPS_MAX_AGE				5000	// max age of GPS data before considering fix lost.
#define GPS_CLOCK_DELAY         1000
#define GPS_MINIMUM_SATELLITES  4
#define GPS_MAXIMUM_HDOP        500		// horizontal dilution of precision in 100ths of a degree


// different commands to set the update rate from once a second (1 Hz) to 10 times a second (10Hz)
#define PMTK_SET_NMEA_UPDATE_1HZ  "$PMTK220,1000*1F"
#define PMTK_SET_NMEA_UPDATE_5HZ  "$PMTK220,200*2C"
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"

#define PMTK_SET_BAUD_9600 "$PMTK251,9600*17"
#define PMTK_SET_BAUD_57600 "$PMTK251,57600*2C"

// turn on only the second sentence (GPRMC)
#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"

// turn on GPRMC and GGA
#define PMTK_SET_NMEA_OUTPUT_RMCGGA "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"

// turn on ALL THE DATA
#define PMTK_SET_NMEA_OUTPUT_ALLDATA "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28"

// turn off output
#define PMTK_SET_NMEA_OUTPUT_OFF "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"


// We need to forward declare the Speedo class, since it contains
// the button class as a member.
class Speedo;


class SpeedoGPS
{
public:
	SpeedoGPS(Speedo * speedo);

	void begin				();
	void loop				();

private:
	void _parse				();
	void _updateClock		();
	void _checkIfStillFixed	();
	void _incrementOdometer	(unsigned long meters);
	void _checkPeakKmph		(int kmph);

	Speedo        *_speedo;
	TinyGPS       _gps;
	unsigned long _clockTimer;
	unsigned long _lastFix;
};

#endif
