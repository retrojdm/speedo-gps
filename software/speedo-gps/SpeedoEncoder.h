// Encoder
// -----------------------------------------------------------------------------
//
// Note:  It's recommended to not attach interrupts on bouncy inputs.
//        https://www.circuitsathome.com/mcu/rotary-encoder-interrupt-service-routine-for-avr-micros#more-5362
//        As a hardware debounce circuit, I've attached a 0.1uF capacitor
//        between:
//          * pin A and GND
//          * pin B and GND
//

#ifndef SPEEDO_ENCODER_H
#define SPEEDO_ENCODER_H

#include <Encoder.h> // https://github.com/PaulStoffregen/Encoder


#define ENCODER_PIN_BTN           2
#define ENCODER_PIN_A             3 // An interrupt pin. Avoid using pins with LEDs attached
#define ENCODER_PIN_B             4 // An interrupt pin. Avoid using pins with LEDs attached
#define ENCODER_STEPS_PER_DETENT  4


// We need to forward declare the Speedo class, since it contains
// the button class as a member.
class Speedo;


class SpeedoEncoder
{
public:
	SpeedoEncoder(Speedo * speedo);
	void begin();
	void loop();

private:
	Speedo  *_speedo;
	Encoder _encoder;
	long    _oldPosition;
	long    _position;

	void	_changeScreen			(int moved);
	void	_changeTimezoneHours	(int moved);
	void	_changeTimezoneMinutes	(int moved);
};

#endif