#include "SpeedoEncoder.h"


// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "speedo.h"


SpeedoEncoder::SpeedoEncoder(Speedo * speedo)
	: _encoder(ENCODER_PIN_A, ENCODER_PIN_B)
{
	_speedo = speedo;
	_oldPosition = 0;
	_position = 0;
}


void SpeedoEncoder::begin()
{

}


// Even though we're using an interrupt to read the rotary encoder,
// We still use the loop to act on it.
void SpeedoEncoder::loop()
{
	_oldPosition	= _position;
	_position		= _encoder.read() / ENCODER_STEPS_PER_DETENT;

	int moved = _position - _oldPosition;

	// Has the encoder moved?
	if (moved != 0)
	{
		switch (_speedo->mode)
		{
		case MODE_VIEW:
		case MODE_GPS_MORE:
			_changeScreen(moved);
			break;

		case MODE_CLOCK_HOURS:
			_changeTimezoneHours(moved);
			break;

		case MODE_CLOCK_MINUTES:
			_changeTimezoneMinutes(moved);
			break;
		}		
	}
}


void SpeedoEncoder::_changeScreen(int moved)
{
	#ifdef ECHO_ENCODER_POSITION
		char text[255];
		sprintf(text, "Encoder:%3d", moved);
		Serial.println(text);
	#endif

	// Remember the old screen, so we can see if anything's changed later.
	int oldScreen = _speedo->settings.screen;
	
	// Wrap the encoder position.
	// Note: The modulus operator doesn't wrap negative numbers correctly,
	// so we add ENCODER_STEPS_PER_DETENT * SCREENS first.
	int newScreen = mod(oldScreen + moved, SCREENS);
	
	// Changed? Save the state.
	if (oldScreen != newScreen)
	{
		this->_speedo->settings.screen = newScreen;
		_speedo->mode = MODE_VIEW;
		_speedo->fram.saveSettings();
	}
}


void SpeedoEncoder::_changeTimezoneHours(int moved)
{
	_speedo->settings.gmtHours = mod(_speedo->settings.gmtHours + 12 + moved, 24) - 12;
	_speedo->fram.saveSettings();
}


void SpeedoEncoder::_changeTimezoneMinutes(int moved)
{
	_speedo->settings.gmtMinutes = mod(_speedo->settings.gmtMinutes + (moved * 30), 60);
	_speedo->fram.saveSettings();
}