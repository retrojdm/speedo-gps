#include "SpeedoSettings.h"


SpeedoSettings::SpeedoSettings()
{
	strcpy(version, SETTINGS_VERSION);
	screen		= 0;
	gmtHours	= 10;	// AEST
	gmtMinutes	= 0;	// AEST
	odometer	= 0;
	tripCounter	= 0;
	kmphPeak	= 0;
}
