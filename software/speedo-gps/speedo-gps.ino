// GPS Speedo
////////////////////////////////////////////////////////////////////////////////
// By Andrew Wyatt
// retrojdm.com
//
// Bugs:
//  - 'speed / peak speed' bug
//  - 'GPS fix' bug
//
#include "Speedo.h"


Speedo speedo;


void setup()
{
	// We pass a pointer to the update function to use in the IntervalTimer (interrupt).
	speedo.begin(updateStepper);

	// If you want to set your odometer, uncomment these two lines and upload/run once.
	// Recomment them and reupload the sketch after.
	//speedo.settings.odometer = 60000000;
	//speedo.fram.saveSettings();
}


void loop()
{
	speedo.loop();
}


////////////////////////////////////////////////////////////////////////////////
// Static callback function

// We can't used std::bind in Arduino :(
// So instead, we create a static function wrapper that calls the member function.
// This wrapper function is passed as the call-back for the IntervalTimer.
void updateStepper()
{
	speedo.stepper.update();
}