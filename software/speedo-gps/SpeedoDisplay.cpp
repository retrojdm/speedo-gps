#include "SpeedoDisplay.h"


// We need to include this here, not in the header. Otherwise there would be
// Circular references.
// It still needs to be included though, since we access some of it's
// members, and their functions.
#include "speedo.h"


SpeedoDisplay::SpeedoDisplay(Speedo * speedo)
	: _display(DISPLAY_PIN_RESET)
{
	_speedo = speedo;
}


void SpeedoDisplay::begin()
{
	// Init the display.
	_display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	_display.clearDisplay();
	_display.drawBitmap(40, 0, datsunLogo48x32, 48, 32, 1);
	_display.display();

	// Kick off the GPS Fix animation
	_gpsFixFrame		= 0;

	// Show the logo for a while.
	_nextRefresh = millis() + DISPLAY_LOGO_DURATION;
}


void SpeedoDisplay::loop()
{
	// Not ready to refresh?
	if (millis() < _nextRefresh) return;
	_nextRefresh = millis() + DISPLAY_REFRESH_DELAY;

	_gpsFixFrame++;
	if (_gpsFixFrame >= DISPLAY_GPSFIX_FRAMES)
		_gpsFixFrame = 0;

	_blink = !_blink;

	// Clear
	_display.clearDisplay();

	// Which screen?
	switch (_speedo->settings.screen)
	{
	case SCREEN_ODOMETER:     _displayOdometer();    break;
	case SCREEN_TRIP_COUNTER: _displayTripCounter(); break;
	case SCREEN_CLOCK:        _displayClock();       break;
	case SCREEN_GPS:          _displayGPS();         break;
	case SCREEN_SPEED:        _displaySpeed();       break;
	case SCREEN_SPEED_PEAK:   _displaySpeedPeak();   break;
	}

	_display.display();
}


////////////////////////////////////////////////////////////////////////////////
// Drawing Functions

void SpeedoDisplay::_printBig(char *text, int x, int y)
{
	// Go through each character in the string...
	int cursorX = x;
	for (unsigned int c = 0; c < strlen(text); c++)
	{
		// Map the character to an index in the array of digit bitmaps.
		char ch = text[c];
		int i = -1;
		if (ch >= '0' && ch <= '9') i = int(ch) - '0';
		else if (ch == ':') i = 10;
		else if (ch == 'N') i = 11;
		else if (ch == 'E') i = 12;
		else if (ch == 'S') i = 13;
		else if (ch == 'W') i = 14;
		else if (ch == '+') i = 15;
		else if (ch == '-') i = 16;

		// Valid index?
		if (i >= 0) _display.drawBitmap(cursorX, y, digits[i], DISPLAY_DIGIT_WIDTH, DISPLAY_DIGIT_HEIGHT, 1);

		// Move the cursor (effectively blank spaces for invalid characters).
		cursorX += DISPLAY_DIGIT_WIDTH;
	}
}


void SpeedoDisplay::_drawLabel(char *label)
{
	_display.setTextSize(1);
	_display.setTextColor(WHITE);
	_display.setCursor((DISPLAY_WIDTH - (strlen(label) * 6)) / 2, 25);
	_display.print(label);
}


void SpeedoDisplay::_drawValue(unsigned long value)
{
	// Big Readout, centered.
	char text[9];
	sprintf(text, "%lu", value);
	_printBig(text, (DISPLAY_WIDTH - strlen(text) * DISPLAY_DIGIT_WIDTH) / 2, 0);
}


void SpeedoDisplay::_drawLocatingAnimation()
{
	_display.drawBitmap(
		(DISPLAY_WIDTH - DISPLAY_GPSFIX_WIDTH) / 2,	// horizontally centered
		0,											// top-aligned
		gpsFix[gpsFixFrames[_gpsFixFrame]],			// which bitmap?
		DISPLAY_GPSFIX_WIDTH,						// width
		DISPLAY_GPSFIX_HEIGHT,						// height
		1);											// colour
}


////////////////////////////////////////////////////////////////////////////////
// Display Functions

void SpeedoDisplay::_displayOdometer()
{
	_drawLabel((char*)"Odometer");
	_drawValue((unsigned long)(_speedo->settings.odometer / 1000));
}


void SpeedoDisplay::_displayTripCounter()
{
	unsigned long tripCounter = _speedo->settings.tripCounter;
	if (tripCounter <= DISPLAY_TRIP_SHOW_METERS)
	{
		_drawValue(tripCounter);
		_drawLabel((char*)"Trip (meters)");
	}
	else
	{
		_drawValue(tripCounter / 1000);
		_drawLabel((char*)"Trip");
	}
}


void SpeedoDisplay::_displayClock()
{
	switch (_speedo->mode)
	{
	case MODE_VIEW:
		_displayClock_View();
		break;

	case MODE_CLOCK_HOURS:
	case MODE_CLOCK_MINUTES:
		_displayClock_Edit();
		break;
	}
}


void SpeedoDisplay::_displayClock_View()
{
	_drawLabel((char*)"Clock");

	if (!_speedo->fix)
	{
		_drawLocatingAnimation();
		return;
	}

	int x = 12; // Clock offset
	int y = 0;
	char text[6];

	int totalMinutes = _speedo->hour * 60 + _speedo->minute;
	totalMinutes += _speedo->settings.gmtHours * 60;
	totalMinutes += _speedo->settings.gmtMinutes;
	totalMinutes = mod(totalMinutes, 1440);

	byte hh = (totalMinutes / 60) % 12;
	if (hh == 0) hh = 12;
	byte mm = totalMinutes % 60;
	byte ss = _speedo->second;

	// HH:MM
	sprintf(text, "%2d:%02d", hh, mm);
	_printBig(text, x, y);

	// SS
	_display.setCursor(x + strlen(text) * DISPLAY_DIGIT_WIDTH + 8, y + DISPLAY_DIGIT_HEIGHT - 8);
	sprintf(text, "%02d", ss);
	_display.print(text);
}


void SpeedoDisplay::_displayClock_Edit()
{
	const static char signChar[3] = { '-', ' ', '+' };

	int x = 12; // Clock offset
	int y = 0;
	int	sign;
	char hh[3];
	char mm[3];
	char text[7];

	_drawLabel((char*)"Timezone");

	// Sign
	sign = 0;
	if (_speedo->settings.gmtHours < 0) sign = -1;
	if (_speedo->settings.gmtHours > 0) sign = 1;

	// HH
	if (_blink && (_speedo->mode == MODE_CLOCK_HOURS))
		strcpy(hh, "__");
	else
	{
		sprintf(hh, "%02d", absolute(_speedo->settings.gmtHours));
	}

	// MM
	if (_blink && (_speedo->mode == MODE_CLOCK_MINUTES))
		strcpy(mm, "__");
	else
		sprintf(mm, "%02d", _speedo->settings.gmtMinutes);

	// [+|-]HH:MM
	sprintf(text, "%c%2s:%2s", signChar[sign + 1], hh, mm);
	_printBig(text, x, y);
}


void SpeedoDisplay::_displayGPS()
{
	_drawLabel((char*)"GPS");

	switch (_speedo->mode)
	{
	case MODE_VIEW:		_displayGPS_View(); break;
	case MODE_GPS_MORE:	_displayGPS_More(); break;
	}	
}


void SpeedoDisplay::_displayGPS_View()
{
	char text[22];

	if (!_speedo->fix)
	{
		_drawLocatingAnimation();
		return;
	}

	_display.setCursor(0, 0);

	// "Lat:"
	dtostrf(_speedo->latitude, 10, 5, text);
	_display.print("Lat:");
	_display.println(text);

	// "Lon:"
	dtostrf(_speedo->longitude, 10, 5, text);
	_display.print("Lon:");
	_display.println(text);

	// "Alt:"
	dtostrf(_speedo->altitude, 4, 0, text);
	_display.print("Alt:");
	_display.print(text);

	// Heading
	// N / E / S / W  
	float angle = _speedo->course;
	if ((angle >= 337.5) || (angle < 22.5)) strcpy(text, "N");
	if ((angle >= 22.5) && (angle < 67.5)) strcpy(text, "NE");
	if ((angle >= 67.5) && (angle < 112.5)) strcpy(text, "E");
	if ((angle >= 112.5) && (angle < 157.5)) strcpy(text, "SE");
	if ((angle >= 157.5) && (angle < 202.5)) strcpy(text, "S");
	if ((angle >= 202.5) && (angle < 247.5)) strcpy(text, "SW");
	if ((angle >= 247.5) && (angle < 292.5)) strcpy(text, "W");
	if ((angle >= 292.5) && (angle < 337.5)) strcpy(text, "NW");
	int x = (DISPLAY_WIDTH - DISPLAY_DIGIT_WIDTH) - ((strlen(text) * DISPLAY_DIGIT_WIDTH) / 2);
	_printBig(text, x, 0);
}


void SpeedoDisplay::_displayGPS_More()
{
	char text[22];

	_display.setCursor(0, 0);

	// "HDOP, Satellites:
	sprintf(text, "HDOP:%4lu  Sats:%5d",
		_speedo->hdop,
		_speedo->satellites);
	_display.println(text);

	// "Fix:"
	sprintf(text, "Fix:%5s  Age:%6lu",
		_speedo->fix ? "yes" : "no",
		_speedo->age);
	_display.println(text);
}


void SpeedoDisplay::_displaySpeed()
{
	char text[22];

	_drawLabel((char*)"KMPH");

	if (!_speedo->fix)
	{
		_drawLocatingAnimation();
		return;
	}

	sprintf(text, "%d", _speedo->kmph);
	int x = (DISPLAY_WIDTH - (strlen(text) * DISPLAY_DIGIT_WIDTH)) / 2;
	_printBig(text, x, 0);
}


void SpeedoDisplay::_displaySpeedPeak()
{
	char text[22];

	_drawLabel((char*)"Peak KMPH");

	sprintf(text, "%d", _speedo->settings.kmphPeak);
	int x = (DISPLAY_WIDTH - (strlen(text) * DISPLAY_DIGIT_WIDTH)) / 2;
	_printBig(text, x, 0);
}
