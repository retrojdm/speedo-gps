#ifndef SPEEDO_FRAM_H
#define SPEEDO_FRAM_H

#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_FRAM_I2C.h"
#include "SpeedoSettings.h"


#define FRAM_SETTINGS_ADDRESS 0
//#define FRAM_SIZE 32768 // bytes


// We need to forward declare the Speedo class, since it contains
// the button class as a member.
class Speedo;


class SpeedoFRAM {
public:
	SpeedoFRAM(Speedo * speedo);
	void  begin();
	void  loop();
	void  saveSettings();
	void  loadSettings();

private:
	Speedo            *_speedo;
	Adafruit_FRAM_I2C _fram;
	bool              _detected;
};

#endif
